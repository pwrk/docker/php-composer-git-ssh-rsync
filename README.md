# php-composer-git-ssh-rsync

CI Docker container image mit PHP und allen Abhängigkeiten für Cypress Tests. 
Kann zum Testen verwendet werden. Es enthält alles notwendige um Tests zu starten.

git, ssh und rsync sind mit an board, weil man das bestimmt für ein
Deployment braucht.

- PHP 7.4, composer
- git
- node 16, yarn

cypress selbst ist nicht im Container vorhanden. Es wird als Abhägigkeit
einem PHP Projekt hinzugefügt. Und zwar per:

`yarn add cypress -D`

Ìm PHP Projek in dem man cypress Tests nutzen will bindet man das Image In seine `.gitlab-ci.yml`
ein per:

`image: registry.gitlab.com/pwrk/docker/php-composer-git-ssh-rsync:main`

Beispiel: https://gitlab.com/pwrk/docker/test-e2e/-/blob/main/.gitlab-ci.yml#L9

## Development

die Entwicklung erfordert ein lokales Doker. Du kannst das Image bauen und
dich als User `cypress``attachen per:

``` bash
git clone https://gitlab.com/pwrk/docker/php-composer-git-ssh-rsync.git
cd php-composer-git-ssh-rsync
yarn build
yarn start
```

im `/home/cypress` findest du das
[run-cypress-local.sh](https://gitlab.com/pwrk/docker/php-composer-git-ssh-rsync/-/blob/main/run-cypress-local.sh).
Damit kannst du ein Beispiel PHP Project im Container auspacken und cypress
tests starten.

## Usage

`yarn build` build the image

`yarn start` start the image as container and attaches /bin/sh

## Enviroment

Im Container ist man User cypress. Cypress binaries sind im PATH

## PHP Modules available

```
[PHP Modules]
calendar
Core
ctype
date
dom
exif
FFI
fileinfo
filter
ftp
gettext
hash
iconv
intl
json
libxml
openssl
pcntl
pcre
PDO
Phar
posix
readline
Reflection
session
shmop
SimpleXML
sockets
sodium
SPL
standard
sysvmsg
sysvsem
sysvshm
tidy
tokenizer
xml
xmlreader
xmlwriter
xsl
Zend OPcache
zip
zlib
[Zend Modules]
Zend OPcache
```
