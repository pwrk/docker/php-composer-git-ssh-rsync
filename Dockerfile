FROM cypress/browsers:node16.13.2-chrome100-ff98 AS base


RUN apt-get update 
RUN apt-get -y install lsb-release joe

RUN wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg && \
	echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/php.list && \
	apt-get update && apt-get -y install php7.4 php7.4-intl php7.4-tidy php7.4-zip php7.4-dom php7.4-xml php7.4-xsl

#RUN apk update && apk -q --no-cache add yarn nodejs xvfb xauth nss libxtst alsa-lib-dev libx11 libnotify-dev mesa-gbm gtk+3.0 gtk+2.0 gcompat chromium

#libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth

#RUN apk -q --no-cache add git freetype libpng libsodium-dev libjpeg-turbo freetype-dev libpng-dev libjpeg-turbo-dev \
#        libzip-dev icu-dev libxml2-dev tidyhtml-dev postgresql-dev libxslt-dev gnupg strace procps lsof shadow \
#        ttf-dejavu ttf-droid ttf-freefont ttf-liberation rabbitmq-c-dev

#RUN docker-php-ext-configure gd --with-freetype --with-jpeg &> /dev/null
#RUN docker-php-ext-install -j$(nproc) pdo_mysql zip intl tidy bcmath sodium sockets xsl soap gd &> /dev/null

#RUN set -ex \
#    && apk add --no-cache --virtual .phpize-deps $PHPIZE_DEPS imagemagick-dev libtool \
#    && export CFLAGS="$PHP_CFLAGS" CPPFLAGS="$PHP_CPPFLAGS" LDFLAGS="$PHP_LDFLAGS" \
#    && pecl install imagick-3.4.4 &> /dev/null \
#    && docker-php-ext-enable imagick \
#    && apk add --no-cache --virtual .imagick-runtime-deps imagemagick \
#    && apk del .phpize-deps

RUN cd && curl --silent --show-error https://getcomposer.org/installer | php && \
    mv ./composer.phar /usr/bin/composer && chmod a+x /usr/bin/composer

##RUN apk add --no-cache --virtual .build-deps $PHPIZE_DEPS && \
##    pecl install xdebug && \
##    pecl install amqp && \
##    docker-php-ext-enable xdebug amqp && \
##    apk del -f .build-deps && \
##        echo "xdebug.mode=debug" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
##        && echo "xdebug.idekey=ide" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
##        && echo "xdebug.max_nesting_level=1024" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

ENV PHP_IDE_CONFIG "serverName=docker-cli"
RUN echo "export PATH=${PATH}:~/node_modules/.bin" >> /etc/profile

#COPY php.ini /usr/local/etc/php/conf.d/

# Create cypress User and install Cypress
RUN groupadd -g 1001 -o cypress
RUN useradd -m -u 1001 -g 1001 -o -s /bin/bash cypress
USER cypress
COPY run-cypress-local.sh /home/cypress/
WORKDIR /home/cypress

# uncomment to check versions during build
RUN php -m
RUN node -v
RUN yarn -v
RUN npm -v
#RUN cat /home/cypress/node_modules/cypress/bin/cypress
#RUN ls -la /home/cypress/node_modules/.bin && id && . /etc/profile && echo $PATH && cypress -v
