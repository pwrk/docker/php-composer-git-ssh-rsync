#!/bin/sh

. /etc/profile

git clone https://gitlab.com/pwrk/docker/test-e2e.git
cd test-e2e
composer install && yarn
